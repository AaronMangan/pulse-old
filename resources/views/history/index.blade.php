@extends('layouts.app')
@section('content')

<!-- This is the basic document search view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Search Documents</div>

                <div class="card-body">
                    <!-- Shows messages if there are any -->
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table">
                        <table class="table table-striped">
                            <thead class="">
                                <tr>
                                    <td>History ID</td>
                                    <td>Document ID</td>
                                    <td>Old Title</td>
                                    <td>New Title</td>
                                    <td>Old Rev</td>                                    
                                    <td>New Rev</td>
                                    <td>Old Discipline</td>
                                    <td>New Discipline</td>
                                    <td>Old Type</td>
                                    <td>New Type</td>
                                </tr>
                            </thead>
                            <tbody class="">
                                @isset($histories)
                                    @foreach($histories as $history)
                                        <tr>
                                            <td>{{ $history->id }}</td>
                                            <td>{{ $history->document_id }}</td>
                                            <td>{{ $history->old_doc_title }}</td>
                                            <td>{{ $history->new_doc_title }}</td>
                                            <td>{{ $history->old_doc_rev }}</td>
                                            <td>{{ $history->new_doc_rev }}</td>
                                            <td>{{ $history->old_doc_discipline }}</td>
                                            <td>{{ $history->new_doc_discipline }}</td>
                                            <td>{{ $history->old_doc_type }}</td>
                                            <td>{{ $history->new_doc_type }}</td>


                                            <!-- For admins a button will be put here to edit history? -->
                                            <!--<td><a href="#" class="btn btn-primary btn-sml">Edit</a></td>--> 
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
    $table->string('document_id');
    $table->string('old_doc_status');
    $table->string('new_doc_status');
    $table->string('old_doc_rev');
    $table->string('new_doc_rev');
    $table->string('old_doc_type');
    $table->string('new_doc_type');
    $table->string('old_doc_discipline');
    $table->string('new_doc_discipline');
    $table->string('old_doc_title');
    $table->string('new_doc_title');
    $table->string('old_doc_attachment')->nullable();
    $table->string('new_doc_attachment')->nullable();
    $table->string('updated_by_user');

-->