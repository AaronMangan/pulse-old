<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary text-white">{{ $message->subject }}</div>
            <div class="card-body">
                {{ $message->body }}
                <br>
                <!--<button action="{{ url('mark/' . $message->id) }}" method="POST" class="btn btn-primary">Mark as Read</button>-->
                <form action="{{ url('mark/' . $message->id) }}" method="post">@csrf<button class="btn btn-primary" type="submit">Mark as Read</button></form>
            </div>
        </div>
    </div>
</div>
<br>