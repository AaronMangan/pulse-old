<!-- Messages Page -->
@include('layouts.app')
@section('content')
    @include('flash-message');
@endsection
@foreach($messages as $message)
    @if($message->isExpired($message))
    @else
        @include('messages.template', ['messages' => 'messages'])
    @endif
@endforeach