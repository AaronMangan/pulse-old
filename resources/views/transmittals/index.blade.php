@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            Transmittals - Outgoing
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Transmittal Id</td>
                        <td>Addressed To</td>
                        <td>Document Count</td>
                        <td>Created By</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transmittals ?? '' as $transmittal)
                        <tr>
                            <td>{{ $transmittal->id }}</td>
                            <td>{{ $transmittal->to }}</td>
                            <td>{{ $transmittal->documents }}</td>
                            <td>{{ $transmittal->created_by_user }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection