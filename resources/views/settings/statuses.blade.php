@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document types, only if a user is admin -->
                @if (auth()->user()->IsAdmin)
                    <div class="card">
                        <div class="card-header">Add New Status</div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('add-status') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="doc_type_name">Status Name</label>
                                        <input type="text" class="form-control" id="status_name" name="status_name" placeholder="e.g. Draft.." />
                                        @error('status_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="status_description">Status Description</label>
                                        <input type="text" class="form-control" name="status_description" placeholder="For documents in a draft state..">
                                        @error('status_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Save Status</button>
                            </form>
                        </div>
                    </div>
                    <br>
                @else 
                @endif
                <!-- Shows the existing document types in a table -->
                <div class="card">
                    <div class="card-header">Current Statuses</div>
                    <div class="card-body">
                        @isset($statuses)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Status Id</td>
                                        <td>Status Name</td>
                                        <td>Status Description</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td>Edit</td>
                                        @else
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($statuses as $status)
                                    <tr>
                                        <td>{{ $status->id }}</td>
                                        <td>{{ $status->status_name }}</td>
                                        <td>{{ $status->status_description }}</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td><a href="{{ url('edit-status/'. $status->id) }}" class="btn btn-primary btn-sml">Edit</a></td>
                                        @else
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- Links for pagination -->
                            {!! $statuses->render() !!}
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection