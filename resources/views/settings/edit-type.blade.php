@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document types -->
                <div class="card">
                    <div class="card-header">Add New Type</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('save-type/' . $type->id) }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6e">
                                    <label for="doc_type_name">Type Name</label>
                                    <input type="text" class="form-control" id="doc_type_name" name="doc_type_name" value="{{ $type->doc_type_name }}" />
                                    @error('doc_type_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10">
                                    <label for="doc_type_description">Type Description</label>
                                    <input type="text" class="form-control" name="doc_type_description" value="{{ $type->doc_type_description }}"/>
                                    @error('doc_type_description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{ url('delete-type/'. $type->id) }}" class="btn btn-danger">Delete</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
