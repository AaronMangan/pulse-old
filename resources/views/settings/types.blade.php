@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document types, only if a user is admin -->
                @if (auth()->user()->IsAdmin)
                    <div class="card">
                        <div class="card-header">Add New Type</div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('add-type') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="doc_type_name">Type Name</label>
                                        <input type="text" class="form-control" id="doc_type_name" name="doc_type_name" placeholder="e.g. Report" />
                                        @error('doc_type_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="doc_typedescription">Type Description</label>
                                        <input type="text" class="form-control" name="doc_type_description" placeholder="Describe the type..">
                                        @error('doc_type_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Save Type</button>
                            </form>
                        </div>
                    </div>
                    <br>
                @else 
                @endif
                <!-- Shows the existing document types in a table -->
                <div class="card">
                    <div class="card-header">Current Document Types</div>
                    <div class="card-body">
                        @isset($doc_types)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Type Id</td>
                                        <td>Type Name</td>
                                        <td>Type Description</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td>Edit</td>
                                        @else
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($doc_types as $type)
                                    <tr>
                                        <td>{{ $type->id }}</td>
                                        <td>{{ $type->doc_type_name }}</td>
                                        <td>{{ $type->doc_type_description }}</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td><a href="{{ url('edit-type/'. $type->id) }}" class="btn btn-primary btn-sml">Edit</a></td>
                                        @else
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                <!-- Links for pagination -->
                                {!! $doc_types->render() !!}
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection