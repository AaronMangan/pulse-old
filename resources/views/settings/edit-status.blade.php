@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document Statuses -->
                <div class="card">
                    <div class="card-header">Edit Status</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('save-status/' . $status->id) }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="status_name">Status Name</label>
                                    <input type="text" class="form-control" id="status_name" name="status_name" value="{{ $status->status_name }}" />
                                    @error('status_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10">
                                    <label for="status_description">Status Description</label>
                                    <input type="text" class="form-control" name="status_description" value="{{ $status->status_description }}"/>
                                    @error('status_description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{ url('delete-status/'. $status->id) }}" class="btn btn-danger">Delete</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
