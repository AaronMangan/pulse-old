@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document types -->
                <div class="card">
                    <div class="card-header">Edit Discipline</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('save-discipline/' . $discipline->id) }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6e">
                                    <label for="doc_discipline_name">Discipline Name</label>
                                    <input type="text" class="form-control" id="doc_discipline_name" name="doc_discipline_name" value="{{ $discipline->doc_discipline_name }}" />
                                    @error('doc_type_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10">
                                    <label for="doc_type_description">Type Description</label>
                                    <input type="text" class="form-control" name="doc_discipline_description" value="{{ $discipline->doc_discipline_description }}"/>
                                    @error('doc_type_description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{ url('delete-discipline/'. $discipline->id) }}" class="btn btn-danger">Delete</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection