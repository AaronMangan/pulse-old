@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                <!-- Show a card to add new document types, only if a user is admin -->
                @if (auth()->user()->IsAdmin)
                    <div class="card">
                        <div class="card-header">Add New Discipline</div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('add-discipline') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="doc_type_name">Discipline Name</label>
                                        <input type="text" class="form-control" id="doc_discipline_name" name="doc_discipline_name" placeholder="e.g. Mechanical" />
                                        @error('doc_discipline_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="doc_dicipline_description">Discipline Description</label>
                                        <input type="text" class="form-control" name="doc_discipline_description" placeholder="Discipline description..">
                                        @error('doc_type_description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Save Discipline</button>
                            </form>
                        </div>
                    </div>
                    <br>
                @else 
                @endif
                <!-- Shows the existing document types in a table -->
                <div class="card">
                    <div class="card-header">Current Document Disciplines</div>
                    <div class="card-body">
                        @isset($disciplines)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>Discipline Id</td>
                                        <td>Discipline Name</td>
                                        <td>Discipline Description</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td>Edit</td>
                                        @else
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($disciplines as $discipline)
                                    <tr>
                                        <td>{{ $discipline->id }}</td>
                                        <td>{{ $discipline->doc_discipline_name }}</td>
                                        <td>{{ $discipline->doc_discipline_description }}</td>
                                        @if(auth()->user()->IsAdmin)
                                            <td><a href="{{ url('edit-discipline/'. $discipline->id) }}" class="btn btn-primary btn-sml">Edit</a></td>
                                        @else
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!--
$table->string('doc_discipline_name');
$table->string('doc_discipline_description');
$table->longText('doc_discipline_notes');
$table->integer('added_by_user');
$table->integer('sort_order');
-->