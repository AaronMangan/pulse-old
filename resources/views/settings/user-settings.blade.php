@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <div class="row justify-content-center">    
            <div class="col-md-8">
                @include('flash-message')
                
                <!-- Show a card to add new document types -->
                <div class="card">
                    <div class="card-header bg-primary text-white">User Preferences for {{ $user->name }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('save-prefs') }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="paginate_results_documents">Document Search Results per Page</label>
                                    <input type="number" class="form-control" name="paginate_results_documents" value="{{ $array->paginate_results_documents }}" data-toggle="tooltip" data-placement="top" title="Results per page for document search"/>
                                    @error('paginate_results_documents')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="paginate_results_other">Number of Other Items Per Page</label>
                                    <input type="number" class="form-control" name="paginate_results_other" value="{{ $array->paginate_results_other }}" data-toggle="tooltip" data-placement="top" title="Results per page for user list, etc"/>
                                    @error('paginate_results_documents')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    
                                </div>
                            </div>

                            <!-- Checkbox to allow user to collect history data -->
                            <span class="d-block p-2 bg-white text-primary">
                                <div class="form-row">
                                    <div class="form-check">
                                        <div class="togglebutton">
                                            <label>
                                            @if ($array->collect_user_history === 1)
                                                <input type="checkbox" name="collect_user_history" checked="" aria-describedby="info-block">
                                            @else
                                                <input type="checkbox" name="collect_user_history" {{ old('collect_user_history') ? 'checked' : '' }} aria-describedby="info-block">
                                            @endif
                                                Collect User History
                                            </label>
                                            <small id="info-block" class="form-text text-muted">
                                                Logs actions in history table.
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </span>
                            <br>
                            <!-- Buttons -->
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{ route('reset-prefs') }}" class="btn btn-danger">Reset Preferences</a>
                        </form>
                    </div>
                </div>
                <br>
                <!-- Passport Vue Components -->
                <passport-clients></passport-clients>
                <br>
                <passport-authorized-clients></passport-authorized-clients>
                <br>
                <passport-personal-access-tokens></passport-personal-access-tokens>
            </div>
        </div>
    </div>
@endsection