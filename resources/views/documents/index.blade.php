@extends('layouts.app')
@section('content')

<!-- This is the basic document search view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
        @include('flash-message')
            <!-- Search Card -->
            <div class="card">
                <div class="card-header">
                    Search For Documents
                </div>

                <!-- Card body with search form -->
                <div class="card-body">
                    <form action="{{ route('doc-query') }}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" placeholder="Search for?" name="doc_query"/>
                            </div>
                            <!-- Form submit button -->
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-primary">Search</button>
                                <a href="{{ route('add-document') }}" class="btn btn-primary">New Document</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>    <!-- br element for spacing. Feels uugh but I don't know any other way -->
            <!-- Document Card -->
            <div class="card">

                <!-- Card header -->
                <div class="card-header">Documents</div>

                <!-- Card body -->
                <div class="card-body">
                    <div class="table">
                        <table class="table table-striped">
                            <thead class="">
                                <tr>
                                    <td>Document Number</td>
                                    <td>Rev</td>
                                    <td>Title</td>
                                    <td>Discipline</td>
                                    <td>Tools</td>
                                </tr>
                            </thead>
                            <tbody class="">
                                @isset($docs)
                                    @foreach($docs as $doc)
                                        <tr>
                                            <td>{{ $doc->id }}</td>
                                            <td>{{ $doc->doc_rev }}</td>
                                            <td>{{ $doc->doc_title }}</td>
                                            <td>{{ $doc->doc_discipline }}</td>
                                            <td><a href="{{ url('edit-document/'.$doc->id) }}" class="btn btn-primary btn-sml">Edit</a></td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
                {!! $docs->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection