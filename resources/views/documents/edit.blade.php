@extends('layouts.app')
@section('content')
<!-- Display error messages -->
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<!-- This is the basic document search view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Edit Document</div>
                <div class="card-body">
                    @isset($doc)
                        <form method="POST" enctype="multipart/form-data" action="{{ url('update-document/'.$doc->id)}}">
                        @csrf
                            <div class="form-row">
                                <!-- Disciplines -->
                                <div class="form-group col-md-6">
                                    <label for="doc_discipline">Discipline</label>
                                    <select id="doc-discipline" name="doc_discipline" class="form-control">
                                        <option disabled selected>{{ $sel_discipline->doc_discipline_name }}</option>
                                        @isset($disciplines)
                                            @foreach($disciplines as $discipline)
                                                <option value="{{ $discipline->id }}">{{ $discipline->doc_discipline_name }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                                <!-- Types -->
                                <div class="form-group col-md-6">
                                    <label for="doc_type">Type</label>
                                    <select id="doc_type" name="doc_type" class="form-control">
                                        <!-- Insert here the dropdown options for doc type from the db -->
                                        <option disabled selected>{{ $sel_type->doc_type_name }}</option>
                                        @isset($types)
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->doc_type_name }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>

                            <!-- Document Title -->
                            <div class="form-group">
                                <label for="doc-title">Document Title</label>
                                <input type="text" class="form-control" id="doc-title" name="doc_title" placeholder="Title..." value="{{ $doc->doc_title }}" />
                            </div>

                            <div class="form-row">
                                <!-- Document Status -->
                                <div class="form-group col-md-4">
                                    <label for="docStatus">Status</label>
                                    <select id="docStatus" name="doc_status" id="doc_status" class="form-control">
                                        <option disabled selected>{{ $sel_status->status_name }}</option>
                                            @isset($statuses)
                                                @foreach($statuses as $status)
                                                    <option value="{{ $status->id }}">{{ $status->status_name }}</option>
                                                @endforeach
                                            @endisset
                                    </select>
                                </div>

                                <!-- Revision -->
                                <div class="form-group col-md-4">
                                    <label for="docRevision">Revision</label>
                                    <select id="docRevision" name="doc_revision" class="form-control">
                                        <option disabled selected>{{ $doc->doc_rev }}</option>
                                            @isset($revs)
                                                @foreach($revs as $rev)
                                                    <option value="{{ $rev->id }}">{{ $rev->rev_name }}</option>
                                                @endforeach
                                            @endisset
                                    </select>
                                </div>

                                <!-- File Attachment -->
                                <div class="form-group col-md-4">
                                    <label for="file-input">Attach File</label>
                                    <input type="file" id="file-input" name="file-storage-name" class="form-control" />
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary">Save Document</button>
                        </form>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
// Standard fields...
// $table->bigIncrements('id');
// $table->timestamps();
// // Custom fields...
// $table->string('doc_class');
// $table->string('doc_discipline');
// $table->string('doc_type');
// $table->string('doc_rev');
// $table->string('doc_title');
// $table->json('metadata');
 -->