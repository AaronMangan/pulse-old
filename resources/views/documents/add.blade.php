@extends('layouts.app')
@section('content')
<!-- Display error messages -->
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<!-- This is the basic document search view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Add New Document</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('save-document')}}">
                    {{ csrf_field() }}
                        <div class="form-row">
                            <!-- Disciplines -->
                            <div class="form-group col-md-6">
                                <label for="doc_discipline">Discipline</label>
                                <select id="doc_discipline" name="doc_discipline" class="form-control">
                                    <option selected>Choose...</option>
                                    @isset($disciplines)
                                        @foreach($disciplines as $discipline)
                                            <option>{{ $discipline->doc_discipline_name }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                            <!-- Types -->
                            <div class="form-group col-md-6">
                                <label for="doc_type">Type</label>
                                <select id="doc_type" name="doc_type" class="form-control">
                                    <!-- Insert here the dropdown options for doc type from the db -->
                                    <option selected>Choose...</option>
                                    @isset($types)
                                        @foreach($types as $type)
                                            <option>{{ $type->doc_type_name }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <!-- Document Title -->
                        <div class="form-group">
                            <label for="doc-title">Document Title</label>
                            <input type="text" name="doc_title" class="form-control" id="doc-title" placeholder="Title...">
                        </div>

                        <div class="form-row">
                            <!-- Document Status -->
                            <div class="form-group col-md-4">
                                <label for="docStatus">Status</label>
                                <select id="docStatus" name="doc_status" class="form-control">
                                    <option selected>Choose...</option>
                                        @isset($statuses)
                                            @foreach($statuses as $status)
                                                <option>{{ $status->status_name }}</option>
                                            @endforeach
                                        @endisset
                                </select>
                            </div>

                            <!-- Revision -->
                            <div class="form-group col-md-4">
                                <label for="docRevision">Revision</label>
                                <select id="docRevision" name="doc_rev" class="form-control">
                                    <option selected>Choose...</option>
                                        @isset($revs)
                                            @foreach($revs as $rev)
                                                <option>{{ $rev->rev_name }}</option>
                                            @endforeach
                                        @endisset
                                </select>
                            </div>

                            <!-- File Attachment -->
                            <div class="form-group col-md-4">
                                <label for="file-input">Attach File</label>
                                <input type="file" id="file-input" class="form-control" />
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary">Add Document</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- 
// Standard fields...
// $table->bigIncrements('id');
// $table->timestamps();
// // Custom fields...
// $table->string('doc_class');
// $table->string('doc_discipline');
// $table->string('doc_type');
// $table->string('doc_rev');
// $table->string('doc_title');
// $table->json('metadata');
 -->