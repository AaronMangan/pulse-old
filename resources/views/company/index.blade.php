@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
        @include('flash-message')
            
            <!-- Card to create new company -->
            <div class="card">
                <div class="card-header">Company Options</div>
                <div class="card-body">
                    <form action="{{ url('new-company') }}" method="get">@csrf<button class="btn btn-primary" type="submit">New Company</button></form>
                </div>
            </div>
            <br>

            <!-- Card to index companies -->
            <div class="card">

                <!-- Card header -->
                <div class="card-header">Companies</div>

                <!-- Card body -->
                <div class="card-body">
                    <div class="table">
                        <table class="table table-striped">
                            <thead class="">
                                <tr>
                                    <td>Company Id</td>
                                    <td>Company Name</td>
                                    <td>Company Address 1</td>
                                    <td>Company Address 2</td>
                                    <td>Company Email</td>
                                    <td>Company Contact</td>
                                    <td>Company PostCode</td>
                                    <td>Company Phone</td>
                                    <td>Company State</td>
                                    <td>View</td>
                                    <td>Edit</td>
                                </tr>
                            </thead>
                            <tbody class="">
                                @isset($companies)
                                    @foreach($companies as $company)
                                        <tr>
                                            <td>{{ $company->id }}</td>
                                            <td>{{ $company->company_name }}</td>
                                            <td>{{ $company->company_address_one }}</td>
                                            <td>{{ $company->company_address_two }}</td>
                                            <td>{{ $company->company_email }}</td>
                                            <td>{{ $company->company_contact }}</td>
                                            <td>{{ $company->company_postcode }}</td>
                                            <td>{{ $company->company_phone }}</td>
                                            <td>{{ $company->company_state }}</td>
                                            <td><a href="{{ url('view-company/'. $company->id) }}" class="btn btn-primary btn-sml">View</a></td>
                                            <td><a href="{{ url('edit-company/'. $company->id) }}" class="btn btn-primary btn-sml">Edit</td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>r
@endsection