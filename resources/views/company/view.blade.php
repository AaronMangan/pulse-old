@extends('layouts.app')
@section('content')
<!-- Display error/info messages -->
@include('flash-message')

<!-- This is the company view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">View Company</div>
                <div class="card-body">
                    @isset($company)
                        @csrf
                        <form method="GET" enctype="multipart/form-data" action="{{ url('companies') }}">
                            <!-- Company Name & Contact -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input readonly type="text" class="form-control" id="company_name" name="company_name" value="{{ $company->company_name }}" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="company_address_one">Company Contact</label>
                                        <input type="text" readonly class="form-control" id="company_contact" value="{{ $company->company_contact }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <!-- Company Address One -->
                                <div class="form-group col-md-12">
                                    <label for="company_address_one">Company Address One</label>
                                    <input type="text" readonly class="form-control" id="company_address_one" value="{{ $company->company_address_one }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <!-- Company Address Two -->
                                <div class="form-group col-md-12">
                                    <label for="company_email" class="">Company Address Two</label>
                                    <input type="text" readonly class="form-control" id="company_email" value="{{ $company->company_address_two }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <!-- Company Contact -->
                                <div class="form-group col-md-6">
                                    <label for="company_state" class="">Company State</label>
                                    <input type="text" readonly class="form-control" id="company_state" value="{{ $company->company_state }}">
                                </div>
                                <!-- Company Email -->
                                <div class="form-group col-md-6">
                                    <label for="company_postcode" class="">Company Postcode</label>
                                    <input type="text" readonly class="form-control" id="company_postcode" value="{{ $company->company_postcode }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company_phone" class="">Company Phone</label>
                                    <input type="text" readonly class="form-control" id="company_phone" value="{{ $company->company_phone }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <button type="submit" class="btn btn-primary">Close</button>
                                </div>
                            </div>
                        </form>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!--
/**
 * 'company_name', 
 * 'company_address_one', 
 * 'company_address_two', 
 * 'company_email', 
 * 'company_contact', 
 * 'company_postcode', 
 * 'company_phone',
 * 'company_state',
 */
-->