@extends('layouts.app')
@section('content')

<!-- Display error/info messages -->
@include('flash-message')

<!-- This is the company view -->
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">View Company</div>
                <div class="card-body">
                    @isset($company)
                        <form method="POST" enctype="multipart/form-data" action="{{ url('update-company/'. $company->id) }}">
                            @csrf
                            <!-- Company Name & Contact -->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" class="form-control" id="company_name" name="company_name" value="{{ $company->company_name }}" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="company_contact">Company Contact</label>
                                        <input type="text" class="form-control" name="company_contact" id="company_contact" value="{{ $company->company_contact }}"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Address Row One -->
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="company_address_one">Company Address One</label>
                                    <input type="text" class="form-control" name="company_address_one" id="company_address_one" value="{{ $company->company_address_one }}">
                                </div>
                            </div>
                            <!-- Address Row Two -->
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="company_email" class="">Company Address Two</label>
                                    <input type="text" class="form-control" name="company_address_two" id="company_email" value="{{ $company->company_address_two }}">
                                </div>
                            </div>

                            <!-- Company State, Postcode, Phone , Email-->
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="company_state" class="">Company State</label>
                                    <input type="text" class="form-control" name="company_state" id="company_state" value="{{ $company->company_state }}">
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="company_postcode" class="">Company Postcode</label>
                                    <input type="text" class="form-control" name="company_postcode" id="company_postcode" value="{{ $company->company_postcode }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company_phone" class="">Company Phone</label>
                                    <input type="text" class="form-control" name="company_phone" id="company_phone" value="{{ $company->company_phone }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="company_email" class="">Company Email</label>
                                    <input type="text" class="form-control" name="company_email" id="company_email" value="{{ $company->company_email }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <button type="submit" class="btn btn-primary">Save Details</button>
                                </div>
                                <div class="form-group col-md-3">
                                    <a class="btn btn-danger" method="post" href="{{ url('delete-company/' . $company->id) }}">Delete Me</a>
                                </div>
                            </div>
                        </form>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
