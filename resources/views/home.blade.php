@extends('layouts.app')

@section('content')
<div class="container">
    @include('flash-message')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    
                    <!-- Show any status messages -->
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <!-- Column Cards - Row 1 -->
                    <div class="row">
                        <div class="column">
                            <div class="card">
                                <a href="{{ route('documents') }}" class="btn btn-primary">Documents</a>
                            </div>
                        </div>
                        <div class="column">
                            <div class="card">
                                <a href="{{ route('transmittals') }}" class="btn btn-primary">Transmittals</a>
                            </div>
                        </div>
                        <div class="column">
                            <div class="card">
                                <a href="{{ route('companies') }}" class="btn btn-primary">Companies</a>
                            </div>
                        </div>
                        <div class="column">
                            <div class="card">
                                <a href="{{ route('history') }}" class="btn btn-primary">History</a>
                            </div>
                        </div>
                    </div>
                    <br>
                    @if(Auth::user()->IsAdmin)
                        <!-- Column Cards - Row 2 -->
                        <!-- This row shows admin functions only, as such only admins can see it -->
                        <div class="row">
                            <div class="column">
                                <div class="card">
                                    <a href="{{ route('manage-disciplines') }}" class="btn btn-primary">Disciplines</a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="card">
                                    <a href="{{ route('manage-types') }}" class="btn btn-primary">Types</a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="card">
                                    <a href="{{ route('manage-statuses') }}" class="btn btn-primary">Statuses</a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="card">
                                    <a href="{{ route('manage-users') }}" class="btn btn-primary">Users</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@endsection
