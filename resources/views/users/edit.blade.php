@extends('layouts.app')
@section('content')
    @if(isset($user))
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Card markup -->
            <div class="card">

                <!-- Card header -->
                <div class="card-header">
                    Edit User: {{ $user->name }}
                </div>
                
                <!-- Card body with fields to edit the user -->
                <div class="card-body">
                    <!-- Form to let the user change details of the user -->
                    <form action="{{ route('update-user', $user->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">User Name:</label>
                            <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Enter user name" value="{{ $user->name }}">
                        </div>

                        <!-- Email address -->
                        <div class="form-group">
                            <label for="email">Email Address:</label>
                            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter an email address" value="{{ $user->email }}">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="togglebutton">
                            <label>
                            @if ($user->IsAdmin === 1)
                                <input type="checkbox" name="admin" checked="">
                            @else
                                <input type="checkbox" name="admin" {{ old('admin') ? 'checked' : '' }}>
                            @endif
                                Administrator
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    @else
        @markdown('**User Not Found...')
    @endif
@endsection