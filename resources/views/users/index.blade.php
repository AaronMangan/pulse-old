@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
            @include('flash-message')
                <div class="card">
                    <div class="card-header">
                        User Index
                    </div>
                    <div class="card-body">
                        @isset($users)
                            <table class="table table-striped">
                                <thead class="">
                                    <tr>
                                        <td>Id</td>
                                        <td>User Name</td>
                                        <td>User Email</td>
                                        <td>Admin</td>
                                        <td>Edit</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->IsAdmin }}</td>
                                            <td><a href="{{ url('edit-user/' . $user->id) }}" class="btn btn-primary">Edit</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>  
                        @else
                        No registered users.
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection