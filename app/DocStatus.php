<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocStatus extends Model
{
    // Specify which table to look for data from...
    protected $table = 'status';

    // Template for a document status object...
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_name', 'status_description', 'added_by_user', 'status_notes',
    ];
}

/*
    $table->string('status_name');
    $table->string('status_description');
    $table->integer('added_by_user');
    $table->longText('status_notes');
*/