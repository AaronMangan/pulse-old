<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_type_name', 'doc_type_description', 'added_by_user', 'sort_order', 'type_notes',
    ];
}

/**
 * 
 *           $table->bigIncrements('id');
 *           $table->timestamps();
 *           $table->string('doc_type_name');
 *           $table->string('doc_type_description');
 *           $table->integer('added_by_user');
 *           $table->integer('sort_order');
 *           $table->longText('type_notes');
 */