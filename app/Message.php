<?php

/**
 * Message model. Holds properties and methds for the message object.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Message;
use Carbon\Carbon;

class Message extends Model
{
    /**
     * Meta casts.
     */
    protected $casts = [
        'seen_by' => 'array',
    ];

    /**
     * Mass assignable fields.
     */
    protected $fillable = [
        'subject', 'body', 'expires', 'status', 'seen_by'
    ];

    /**
     * Compares if the current date/time is past the expired value of the message.
     */
    public function isExpired(Message $message)
    {
        // Get the current DateTime.
        $now = Carbon::now();

        // Compare the two dates:
        if($message->expires >= $now)
        {
            // Message is still active.
            return false;
        }
        else
        {
            // Message has past the expiration date.
            return true;
        }

    }

}
