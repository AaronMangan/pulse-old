<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_discipline_name', 'doc_discipline_description', 'added_by_user', 'sort_order', 'doc_discipline_notes',
    ];
}

/*
    $table->string('doc_discipline_name');
    $table->string('doc_discipline_description');
    $table->longText('doc_discipline_notes');
    $table->integer('added_by_user');
    $table->integer('sort_order');
*/