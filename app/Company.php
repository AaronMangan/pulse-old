<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    // Document Model.
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 
        'company_address_one', 
        'company_address_two', 
        'company_email', 
        'company_contact', 
        'company_postcode', 
        'company_phone',
        'company_state',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'verified' => 'datetime',
    ];
}
