<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    // Document Model.
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_class', 'doc_discipline', 'doc_type', 'doc_rev', 'doc_title', 'metadata', 'doc_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '', '',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'verified' => 'datetime',
    ];
}

/**
 * // Standard fields...
 * $table->bigIncrements('id');
 * $table->timestamps();
 * $table->string('doc_class');
 * $table->string('doc_discipline');
 * $table->string('doc_type');
 * $table->string('doc_rev');
 * $table->string('doc_title');
 * $table->json('metadata');
 */