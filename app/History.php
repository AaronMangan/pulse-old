<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    // Specify which table to use...
    protected $table = 'document_history';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document_id',
        'old_doc_status',
        'new_doc_status',
        'old_doc_rev',
        'new_doc_rev',
        'old_doc_type',
        'new_doc_type',
        'old_doc_discipline',
        'new_doc_discipline',
        'old_doc_title',
        'new_doc_title',
        'old_doc_attachment',
        'new_doc_attachment',
        'updated_by_user',
    ];
}