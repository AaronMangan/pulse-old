<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Show the users index view, available only to admin users.
     */
    public function index(Request $request)
    {
        // Create a variable called users to hold a collection of all registered users in the DB.
        $users = User::all();

        // Return the index view with the collection to display them.
        return view('users.index', compact('users'));
    }

    /**
     * Edit a user's details in the database
     */
    public function edit(Request $request, $id)
    {
        // Retrieve the user model from the database.
        $user = User::findOrFail($id);

        // Return the edit user view to be displayed.
        return view('users.edit', compact('user'));
    }

    /**
     * Delete a user from the DB. This delete is permenant and users cannot be restored.
     */
    public function delete($id)
    {
        // Retrieve the user model instance.
        $user = User::findOrFail($id);

        $user->delete();
        return redirect('manage-users')->with('success', 'User ' . $user->name . ' successfully deleted from the DB');
    }

    /**
     * Update an existing user in the database.
     */
    public function update(Request $request, $id)
    {   
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:250',
        ]);

        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        
        // Logic here to make user administrator or not based on the form data submitted.
        $isAdmin = $request->input('admin');
        if($isAdmin == 'on')
        {
            $user->IsAdmin = '1';   // User to be made an administrator.
        }
        else
        {
            $user->IsAdmin = '0';   // User is a normal user.
        }
        // Save the newly created discipline.
        $user->save();

        // Redirect the user back to the status management page.
        return redirect('manage-users')->with('success', 'User ' . $user->name . ' updated successfully!');
    }

    /**
     * Create a new user.
     */
    public function create(Request $request)
    {
        // Create a new User Model.
        $user = new User();

        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:250|unique:users',
            'password' => 'required|min:8|string',
        ]);

        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect('manage-users')->with('success', 'User added successfully!');

    }

    /**
     * Show the new user creation form.
     */
    public function new()
    {
        return view('users.create');
    }
}
