<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocType;
use Illuminate\Support\Facades\Validator;
use App\Discipline;
use App\DocStatus;
use DB;
use App\User;

class SettingsController extends Controller
{
    /**
     * Document Type Methods
     * 
     * Methods for interacting with document types.
     */

    /**
     * Show the view that allows the user to edit doc types.
     */
    public function types(Request $request)
    {
        // Retrieve document types, paginated by ten. 0-9.
        $doc_types = DocType::paginate(9);
        return view('settings.types', compact('doc_types'));
    }

    /**
     * Adds a newly specified document type to the DB.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addType(Request $request)
    {
        // Show all variables in the request.
        //dd($request->all());
        $user = auth()->user();

        // Validate the user input.
        $v = Validator::make($request->all(), [
            'doc_type_name' => 'required|string|max:30',
            'doc_type_description' => 'required|string|max:100',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
        
        $new_type = new DocType();
        $new_type->doc_type_name = $request->input('doc_type_name');
        $new_type->doc_type_description = $request->input('doc_type_description');
        $new_type->added_by_user = $user->id;
        $new_type->sort_order = '0';
        $new_type->type_notes = '';

        $new_type->save();
        
        return redirect('manage-types');
    }

    /**
     * Shows a view that allows the user to update a document type.
     * 
     * @param integer $id   The id of the type to edit.
     * @return \Illuminate\Http\Response
     */
    public function editType($id)
    {
        $type = DocType::findOrFail($id);
        return view('settings.edit-type', compact('type'));
    }

    /**
     * Delete the document type from the DB.
     */
    public function deleteType(Request $request, $id)
    {
        // Delete the specified document type.
        $doc_type = Doctype::findOrFail($id);

        $doc_type->delete();
        return redirect('manage-types')->with('success', 'Document type delete successfully!');
    }

    /**
     * Update the document type in the database.
     */
    public function saveType(Request $request, $id)
    {
        $doc_type = DocType::find($id);

        $v = Validator::make($request->all(), [
            'doc_type_name' => 'required|string|max:50',
            'doc_type_description' => 'required|string|max:100',
        ]);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        else
        {
            $doc_type->doc_type_name = $request->input('doc_type_name');
            $doc_type->doc_type_description = $request->input('doc_type_description');
            $doc_type->save();
        }
        
        return redirect('manage-types')->with('success', 'Document type updated successfully!');

    }

    /**
     * Document Discipline Methods.
     * 
     * Methods for interacting with document disciplines.
     */

    /**
     * Show the discipline management page.
     */
    public function disciplines(Request $request)
    {
        $disciplines = Discipline::all();
        // Return the discipline page view with the list of disciplines retrieved from the DB.
        return view('settings.disciplines', compact('disciplines'));
    }

    /**
     * Edit a discipline currently in the DB.
     */
    public function editDiscipline($id)
    {
        $discipline = Discipline::findOrFail($id);

        return view('settings.edit-discipline', compact('discipline'));
    }

    /**
     * Add a new discipline to the DB.
     */
    public function addDiscipline(Request $request)
    {
        // We need the currently logged in user information.
        $user = auth()->user();

        // Validate the user input.
        $v = Validator::make($request->all(), [
            'doc_discipline_name' => 'required|string|max:50',
            'doc_discipline_description' => 'required|string|max:100',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        // Create a new discipline model.
        $discipline = new Discipline();
        $discipline->doc_discipline_name = $request->input('doc_discipline_name');
        $discipline->doc_discipline_description = $request->input('doc_discipline_description');
        $discipline->added_by_user = $user->id;
        $discipline->sort_order = '0';
        $discipline->doc_discipline_notes = 'Created by ' . $user->name;
        $discipline->save();

        return redirect('manage-disciplines')->with('success', 'Discipline added successfully!');
        
    }

    /**
     * Delete an existing discipline from the DB.
     */
    public function deleteDiscipline($id)
    {
        // Find the discipline in the DB.
        $discipline = Discipline::find($id);

        // Once found, delete the discipline.
        $discipline->delete();

        // Redirect the user.
        return redirect('manage-disciplines')->with('success', 'Discipline deleted!');

    }

    /**
     * Save the discipline with updated details.
     */
    public function saveDiscipline(Request $request, $id)
    {
        // Validate the user input.
        $v = Validator::make($request->all(), [
            'doc_discipline_name' => 'required|string|max:50',
            'doc_discipline_description' => 'required|string|max:100',
        ]);
        
        // If the validation fails.
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $discipline = Discipline::findOrFail($id);
        $discipline->doc_discipline_name = $request->input('doc_discipline_name');
        $discipline->doc_discipline_description = $request->input('doc_discipline_description');
        
        // Save the discipline with updated details.
        $discipline->save();
        return redirect('manage-disciplines')->with('success', 'Discipline updated successfully!');
    }

    /**
     * Document Status Methods.
     * 
     * Methods for interacting with the document status model.
     */

    /**
     * Show the list of current statuses to the user
     */
    public function statuses(Request $equest)
    {
        // Get all the statuses in the DB based on the model.
        $statuses = DocStatus::paginate(10);

        return view('settings.statuses', compact('statuses'));
    }

    /**
     * Adds a new status to the DB for use.
     */
    public function addStatus(Request $request)
    {
        // Get the currently logged in user details.
        $user = auth()->user();

        $v = Validator::make($request->all(), [
            'status_name' => 'required|string|max:50',
            'status_description' => 'required|string|max:100',
        ]);

        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $status = new DocStatus();
        $status->status_name = $request->input('status_name');
        $status->status_description = $request->input('status_description');
        $status->added_by_user = $user->id;
        $status->status_notes = 'Created by '. $user->name;
        
        // Save the newly created discipline.
        $status->save();

        // Redirect the user back to the status management page.
        return redirect('manage-statuses')->with('success', 'Status added successfully!');
    }

    /**
     * Edit an existing statuses.
     */
    public function editStatus($id)
    {
        // Find the status in the DB.
        $status = DocStatus::find($id);

        // Return the edit view.
        return view('settings.edit-status', compact('status'));
    }

    /**
     * Save the newly updated status details.
     */
    public function saveStatus(Request $request, $id)
    {
        // Get the currently logged in user details.
        $user = auth()->user();

        $v = Validator::make($request->all(), [
            'status_name' => 'required|string|max:50',
            'status_description' => 'required|string|max:100',
        ]);

        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $status = DocStatus::findOrFail($id);
        $status->status_name = $request->input('status_name');
        $status->status_description = $request->input('status_description');
        $status->status_notes = 'Updated by: '. $user->name;
        
        // Save the newly created discipline.
        $status->save();

        // Redirect the user back to the status management page.
        return redirect('manage-statuses')->with('success', 'Status updated successfully!');
    }

    /**
     * Delete an existing status from the database.
     */
    public function deleteStatus($id)
    {
        // Find the status to be deleted.
        $status = DocStatus::findOrFail($id);

        $status->delete();
        return redirect('manage-statuses')->with('success', $status->status_name . ' deleted successfully!');
    }

    /**
     * Direct the user to the preferences page.
     */
    public function userPrefs(Request $request)
    {
        // Get the current logged in user.
        $user = auth()->user();
        
        // Setup checkbox...
        $settings = DB::table('user_prefs')->where('user_id', $user->id)->get();
        
        //$history = $settings[0]->collect_user_history;
        $array = $settings[0];

        return view('settings.user-settings', compact('user'), compact('array'));
    }

    /**
     * Save user defined preferences to the DB.
     */
    public function savePrefs(Request $request)
    {
        $userHistory = $request->has('collect_user_history') ? true : false;
        
        $id = auth()->user()->id;

        DB::table('user_prefs')
            ->where('user_id', $id)
            ->update([
                'paginate_results_documents' => $request->input('paginate_results_documents'),
                'paginate_results_other' => $request->input('paginate_results_other'),
                'collect_user_history' => $userHistory,
                ]);
        return redirect('home');
    }

    /**
     * Resets the user preferences to their originals values.
     * 
     * @author Aaron Mangan <azza.mangan@gmail.com>
     * @return Redirect Rdirect the user to preferences on completion.
     */
    public function resetPrefs()
    {
        // Get the is for the current user.
        $id = auth()->user()->id;

        // Assign the values back to the recommended amounts/states.
        $paginate_results_documents = 50;
        $paginate_results_other = 50;
        $history = false;

        DB::table('user_prefs')->where('user_id', $id)->update([
            'paginate_results_documents' => $paginate_results_documents,
            'paginate_results_other' => $paginate_results_other,
            'collect_user_history' => $history,
        ]);
        
        return redirect('user-prefs')->with('success', 'Preferences were reset!');

    }
}
