<?php

namespace App\Http\Controllers;

use App\Documents;
use Illuminate\Http\Request;
use App\DocType;
use App\Discipline; 
use App\DocStatus;
use DB;
use Illuminate\Support\Facades\Validator;
use Redirect;
use App\Auth;
use App\History;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Displays the documents index page..
        $docs = Documents::paginate(10);
        // dd($docs);
        return view('documents.index', compact('docs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show the form to add a new document.
        // Get the list of document types...
        $types = DocType::all();
        
        // Get a list of document disciplines...
        $disciplines = Discipline::all();

        // Get a list of document statuses...
        $statuses = DocStatus::all();

        // Get a list of the current revisions in the DB...
        $revs = DB::table('revisions')->get();

        // Return the create document view...
        return view('documents.add', compact('types', 'disciplines', 'statuses', 'revs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get the current logged in user.
        $user = auth()->user();

        // Create a history line to add..
        $history = new History();

        // Assign values. Since this is a new document, the old values will be void but we need to assign them anyway.
        $history->old_doc_status = 'N/A';
        $history->old_doc_rev = 'N/A';
        $history->old_doc_type = 'N/A';
        $history->old_doc_discipline = 'N/A';
        $history->old_doc_title = 'N/A';
        $history->old_doc_attachment = 'N/A';
        $history->updated_by_user = $user->id;


        // Now we need to validate the request data.
        $v = Validator::make($request->all(), [
            'doc_discipline' => 'required|string',
            'doc_status' => 'required|string',
            'doc_title' => 'required|string|min:10',
            'doc_type' => 'required|string',
            'doc_rev' => 'required',
        ]);

        if($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        // Create a new document object which will eventually be saved to the database.
        $document = new Documents();

        // Assign the document object values, based on the user input from the form.
        $document->doc_discipline = $request->input('doc_discipline');
        $document->doc_type = $request->input('doc_type');
        $document->doc_status = $request->input('doc_status');
        $document->doc_rev = $request->input('doc_rev');
        $document->doc_title = $request->input('doc_title');
        $document->added_by_user = $user->id;
        $document->metadata = '{}';

        // Save the document object to the DB.
        $document->save();

        // Now we can take the document object details and assign them to the history object.
        // The history object holds information about document changes.
        $history->document_id = $document->id;
        $history->new_doc_status = $document->doc_status;
        $history->new_doc_rev = $document->doc_rev;
        $history->new_doc_type = $document->doc_type;
        $history->new_doc_discipline = $document->doc_discipline;
        $history->new_doc_title = $document->doc_title;

        // Save the history object to the database. 
        $history->save();
        $documents = Documents::paginate(10);
        return redirect('documents')->with('success', 'Document added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function show(Documents $documents)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function edit(Documents $documents, $id)
    {
        // First, recall the resource from the appropriate table...
        $doc = Documents::find($id);

        // Get the list of document types...
        $types = DocType::all();
        $sel_type = DocType::find($doc->doc_type);

        // Get a list of document disciplines...
        $disciplines = Discipline::all();
        $sel_discipline = Discipline::find($doc->doc_discipline);

        // Get a list of document statuses...
        $statuses = DocStatus::all();
        $sel_status = DocStatus::find($doc->doc_status);

        // Get a list of the current revisions in the DB...
        $revs = DB::table('revisions')->get();

        // Return the edit form with the document data to edit it.
        return view('documents.edit', compact(
            'doc', 
            'sel_discipline', 
            'types', 'sel_type', 
            'disciplines', 
            'sel_status', 
            'statuses', 
            'revs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Documents::find($id);
        
        // Go through and check the values, assigning them if they exist.
        if($request->has('doc_discipline'))
        {
            $document->doc_discipline = $request->doc_discipline;
        }
        if($request->has('doc_type'))
        {
            $document->doc_type = $request->doc_type;
        }
        if($request->has('doc_title'))
        {
            $document->doc_title = $request->doc_title;
        }
        if($request->has('doc_rev'))
        {
            $document->doc_rev = $request->doc_rev;
        }
        if($request->has('doc_status'))
        {
            $document->doc_status = $request->doc_status;
        }
        
        // Finally, save the now document properties.
        $document->save($document->toArray());

        // Get document list to pass to the view.
        $documents = Documents::paginate(10);

        return \Redirect::to('documents')->with('success', 'Update successful!');
        //dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Documents  $documents
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documents $documents)
    {
        //
    }

    /**
     * Search for documents using the terms provided by the user.
     */
    public function search(Request $request)
    {
        // Get the search term specified by the user.
        $query = $request->input('doc_query');

        // Now search the DB using 'LIKE'.
        $docs = Documents::where('doc_title', 'LIKE', '%' . $query. '%')->orWhere('id', '=', $query)->paginate(10);

        // Return the results back to the view, paginated.
        return view('documents.index', compact('docs'));
    }
}

/* Database fields for the documents table...
    $table->string('doc_status');
    $table->string('doc_discipline');
    $table->string('doc_type');
    $table->string('doc_rev');
    $table->string('doc_title');
    $table->integer('added_by_user');
    $table->json('metadata');

    There are other fields that are auto generated in the table like id and the two timestamp fields.
*/