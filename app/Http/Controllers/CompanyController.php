<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Gather the list of companies.
        $companies = Company::all();
        
        // Show the company index.
        return view('company.index', compact('companies', $companies));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Shows the blade template used to create a new company in the DB.
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get the data from the request, validate it and if good, place it in the DB.
        $company = new Company();

        // Validate the user input.
        $v = Validator::make($request->all(), [
            'company_name' => 'required|string|max:100',
            'company_address_one' => 'required|string|max:100',
            'company_address_two' => 'required|string|max:100',
            'company_email' => 'required|email|max:100',
            'company_contact' => 'required|string|max:50',
            'company_postcode' => 'required',
            'company_phone' => 'required|integer',
            'company_state' => 'required|string|max:50',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
        // Now assign the request data to the company
        $company->company_name = $request->input('company_name');
        $company->company_address_one = $request->input('company_address_one');
        $company->company_address_two = $request->input('company_address_two');
        $company->company_email = $request->input('company_email');
        $company->company_contact = $request->input('company_contact');
        $company->company_postcode = $request->input('company_postcode');
        $company->company_phone = $request->input('company_phone');
        $company->company_state = $request->input('company_state');
        
        $company->save();
        return redirect('companies')->with('success', 'New company saved!');
        
    }

    /**
     * Display the specified company.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // First, find the company.
        $company = Company::find($id);
        
        // Return the view that shows a company's details.
        return view('company.view', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Check the user is an admin or not
        $company = Company::find($id);
        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        // Validate the user input.
        $v = Validator::make($request->all(), [
            'company_name' => 'required|string|max:100',
            'company_address_one' => 'required|string|max:100',
            'company_address_two' => '',
            'company_email' => 'required|email|max:100',
            'company_contact' => 'required|string|max:50',
            'company_postcode' => 'required',
            'company_phone' => 'required|integer',
            'company_state' => 'required|string|max:50',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
        // Now assign the request data to the company
        $company->company_name = $request->input('company_name');
        $company->company_address_one = $request->input('company_address_one');
        $company->company_address_two = $request->input('company_address_two');
        $company->company_email = $request->input('company_email');
        $company->company_contact = $request->input('company_contact');
        $company->company_postcode = $request->input('company_postcode');
        $company->company_phone = $request->input('company_phone');
        $company->company_state = $request->input('company_state');
        
        $company->save();
        return redirect('companies')->with('success', 'Company details updated!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find the company specified.
        $company = Company::find($id);
        $company->delete();
        return redirect('companies')->with('success', 'Company deleted successfully!');
    }
}