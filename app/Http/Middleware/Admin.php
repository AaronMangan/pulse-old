<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user() &&  Auth::user()->IsAdmin == 1) 
        {
            return $next($request);
        }

        return redirect('home')->with('error', 'You are not allowed to go there');
        // return $next($request);
    }

    /**
     * Method to return the admin status of a user.
     * 
     * @param \Illuminate\Http\Request  $request
     * @param \App\User $UserId
     * @return mixed
     */
    public function IsAdmin($request, $UserId)
    {
        $user = User::find($userId);
    
        if($user->admin == 1)
        {
            return true;
        }
        return false;
    }
}
