<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Basic route.
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/**
 * DOCUMENT MANAGEMENT ROUTES...
 * 
 */
Route::get('documents', 'DocumentsController@index')->name('documents');
Route::get('add-document', 'DocumentsController@create')->name('add-document');
Route::post('save-document', 'DocumentsController@store')->name('save-document');
Route::get('edit-document/{id}', 'DocumentsController@edit');
Route::post('update-document/{id}', 'DocumentsController@update')->name('update-document/{id}');

/**
 * DOCUMENT SEARCH ROUTES...
 * 
 */
Route::any('doc-query', 'DocumentsController@search')->name('doc-query');


/**
 * DOCUMENT HISTORY ROUTES...
 */
Route::get('history', 'HistoryController@index')->name('history');

/**
 * COMPANY MANAGEMENT ROUTES...
 */
Route::get('companies', 'CompanyController@index')->name('companies');  // Show the companies index.
Route::get('view-company/{id}', 'CompanyController@show')->name('view-company/{id}');
Route::get('edit-company/{id}', 'CompanyController@edit')->name('edit-company/{id}')->middleware('admin');
Route::post('update-company/{id}', 'CompanyController@update')->name('update-company/{id}')->middleware('admin');
Route::get('new-company', 'CompanyController@create')->name('new-company')->middleware('admin');
Route::post('save-company', 'CompanyController@store')->name('save-company')->middleware('admin');
Route::get('delete-company/{id}', 'CompanyController@destroy')->name('delete-company/{id}')->middleware('admin');

/**
 * MANAGE DOCUMENT METADATA ROUTES...
 * These routes are for managing document types, disciplines, revisions.
 */

// Document type management routes.
Route::get('manage-types','SettingsController@types')->name('manage-types');
Route::post('add-type', 'SettingsController@addType')->name('add-type')->middleware('admin');
Route::get('edit-type/{id}', 'SettingsController@editType')->name('edit-type/{id}')->middleware('admin');
Route::get('delete-type/{id}', 'SettingsController@deleteType')->name('delete-type/{id}')->middleware('admin');
Route::post('save-type/{id}', 'SettingsController@saveType')->name('save-type/{id}')->middleware('admin');

// Document discipline management routes.
Route::get('manage-disciplines', 'SettingsController@disciplines')->name('manage-disciplines');
Route::post('add-discipline', 'SettingsController@addDiscipline')->name('add-discipline')->middleware('admin');
Route::get('edit-discipline/{id}', 'SettingsController@editDiscipline')->name('edit-discipline/{id}')->middleware('admin');
Route::get('delete-discipline/{id}', 'SettingsController@deleteDiscipline')->name('delete-discipline')->middleware('admin');
Route::post('save-discipline/{id}', 'SettingsController@saveDiscipline')->name('save-discipline')->middleware('admin');

// Document status management routes.
Route::get('manage-statuses', 'SettingsController@statuses')->name('manage-statuses');
Route::post('add-status', 'SettingsController@addStatus')->name('add-status')->middleware('admin');
Route::get('edit-status/{id}', 'SettingsController@editStatus')->name('edit-status/{id}')->middleware('admin');
Route::post('save-status/{id}', 'SettingsController@saveStatus')->name('save-status/{id}')->middleware('admin');
Route::get('delete-status/{id}', 'SettingsController@deleteStatus')->name('delete-status/{id}')->middleware('admin');

// User management routes.
Route::get('manage-users', 'UsersController@index')->name('manage-users')->middleware('admin');
Route::get('edit-user/{id}', 'UsersController@edit')->name('edit-user/{id}')->middleware('admin');
Route::get('delete-user/{id}', 'UsersController@delete')->name('delete-user/{id}')->middleware('admin');
Route::post('update-user/{id}', 'UsersController@update')->name('update-user')->middleware('admin');
Route::post('create-user', 'UsersController@create')->name('create-user')->middleware('admin');
Route::get('new-user', 'UsersController@new')->name('new-user')->middleware('admin');

// Transmittal management routes.
Route::get('transmittals', 'TransmittalController@index')->name('transmittals');

/**
 * User preferences. Allows user to custmoise their individual settings.
 * 
 */
Route::get('user-prefs', 'SettingsController@userPrefs')->name('user-prefs');
Route::post('save-prefs', 'SettingsController@savePrefs')->name('save-prefs');
Route::get('reset-prefs', 'SettingsController@resetPrefs')->name('reset-prefs');

/**
 * Message routes.
 */
Route::get('msg', 'MessageController@test')->name('msg');
Route::get('messages', 'MessageController@index')->name('messages');
Route::post('mark/{id}', 'MessageController@mark')->name('mark/{id}');