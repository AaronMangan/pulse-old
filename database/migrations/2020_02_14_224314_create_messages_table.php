<?php

/**
 * Creates a table in the DB schema called 'messages' and used to persist message data.
 * 
 * @category Migrations
 * @author Aaron Mangan <aaronmangan@pulse.com>
 * @copyright Aaron Mangan 2020
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            
            /**
             * Auto-generated schema.
             */
            $table->bigIncrements('id');
            $table->timestamps();

            /**
             * Additional schema.
             */
            $table->string('subject');
            $table->text('body');             
            $table->dateTime('expires');
            $table->string('status');
            $table->json('seen_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
