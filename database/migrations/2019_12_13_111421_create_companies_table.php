<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            // System generated fields.
            $table->bigIncrements('id');    // Primary key.
            $table->timestamps();   // Created_at, Updated_at & Deleted_at (if delete is used)

            // Application fields.
            $table->string('company_name');
            $table->string('company_address_one');
            $table->string('company_address_two')->nullable();
            $table->string('company_email');
            $table->string('company_contact')->nullable();
            $table->string('company_postcode')->nullable();
            $table->string('company_phone');
            $table->string('company_state');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}

/**
 * 'company_name', 
 * 'company_address_one', 
 * 'company_address_two', 
 * 'company_email', 
 * 'company_contact', 
 * 'company_postcode', 
 * 'company_phone',
 * 'company_state',
 */