<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserPrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Modify the user preferences table to include new preferences.
        Schema::table('user_prefs', function (Blueprint $table) {
            
            // Add a column to allow the user to turn on or off searching history with document search.
            $table->boolean('search_history')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_prefs', function (Blueprint $table) {
            //
        });
    }
}
