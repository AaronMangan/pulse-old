<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('document_id');
            $table->string('old_doc_status');
            $table->string('new_doc_status');
            $table->string('old_doc_rev');
            $table->string('new_doc_rev');
            $table->string('old_doc_type');
            $table->string('new_doc_type');
            $table->string('old_doc_discipline');
            $table->string('new_doc_discipline');
            $table->string('old_doc_title');
            $table->string('new_doc_title');
            $table->string('old_doc_attachment')->nullable();
            $table->string('new_doc_attachment')->nullable();
            $table->string('updated_by_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_history');
    }
}