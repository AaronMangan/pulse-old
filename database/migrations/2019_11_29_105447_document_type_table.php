<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DocumentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create a database table to hold the document type options...
        // This table can be updated by an admin...
        Schema::create('doc_types', function (Blueprint $table) {
            
            // Standard fields...
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('doc_type_name');
            $table->string('doc_type_description');
            $table->integer('added_by_user');
            $table->integer('sort_order');
            $table->longText('type_notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop if the table exists...
        Schema::dropIfExists('doc_types');
    }
}
