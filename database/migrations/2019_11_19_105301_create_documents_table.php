<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            
            // Standard fields...
            $table->bigIncrements('id');
            $table->timestamps();

            // Custom fields...
            $table->string('doc_status');
            $table->string('doc_discipline');
            $table->string('doc_type');
            $table->string('doc_rev');
            $table->string('doc_title');
            $table->integer('added_by_user');
            $table->json('metadata');   // This is a special field that allows for custom fields. Meta-data fields can be configured as per user requirements.
            $table->string('file-storage-name')->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
