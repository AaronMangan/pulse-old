<?php

use Illuminate\Database\Seeder;

class DocStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add some data to the DocStatus table...
        DB::table('status')->insert([
            'status_name' => 'DRAFT',
            'status_description' => 'Being prepared/edited. Not approved or released',
            'added_by_user' => '0',
            'status_notes' => 'For documents still being prepared or edited. Approved documents with a status of draft are being revised.'
        ]);

        DB::table('status')->insert([
            'status_name' => 'APPROVED',
            'status_description' => 'For approved documents',
            'added_by_user' => '0',
            'status_notes' => 'For documents finalised, Approved documents may be released or transmitted to other parties.'
        ]);

        DB::table('status')->insert([
            'status_name' => 'SUPERSEDED',
            'status_description' => 'Used when a document is no longer valid and is replaced with another document.',
            'added_by_user' => '0',
            'status_notes' => 'Superseded documents and their content are no longer valid and are replaced by another document.'
        ]);

        DB::table('status')->insert([
            'status_name' => 'OBSOLETE',
            'status_description' => 'Obsolete documents are no longer suitable for use.',
            'added_by_user' => '0',
            'status_notes' => 'Documents that are obsolete are no longer valid. They are for information only.'
        ]);

        DB::table('status')->insert([
            'status_name' => 'REMOVED',
            'status_description' => 'Removed from the database',
            'added_by_user' => '0',
            'status_notes' => 'Removed from the database so they no longer appear in search results.'
        ]);
    }
}

/*
    $table->string('status_name');
    $table->string('status_description');
    $table->integer('added_by_user');
    $table->longText('status_notes');
*/