<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed some data in the company table...
        DB::table('companies')->insert([
            'company_name' => 'Company Inc.',
            'company_address_one' => '123 Success Ave',
            'company_address_two' => 'Dedication',
            'company_email' => 'success@company.mail.com',
            'company_contact' => 'Adam Burkowitz',
            'company_postcode' => '0100101',
            'company_phone' => '1800999999',
            'company_state' => 'Innovation',
        ]);
    }
}


/**
 * 'company_name', 
 * 'company_address_one', 
 * 'company_address_two', 
 * 'company_email', 
 * 'company_contact', 
 * 'company_postcode', 
 * 'company_phone',
 * 'company_state',
 */