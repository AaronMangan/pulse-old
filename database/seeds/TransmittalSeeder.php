<?php

use Illuminate\Database\Seeder;

class TransmittalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert data into the transmittals table.
        DB::table('transmittals')->insert([
            'to' => 'Company ONE',
            'documents' => "{ 
               
           }",
             'created_by_user' => 'Aaron Mangan',
             'filename' => 'file.zip',
        ]);
    }
}
