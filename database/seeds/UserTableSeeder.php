<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed an admin user to start with.
        DB::table('users')->insert([
            'name' => 'administrator',
            'email' => 'admin@pulse.com',
            'password' => Hash::make('pulse.test'),
        ]);
    }
}
