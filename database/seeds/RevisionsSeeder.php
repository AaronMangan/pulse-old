<?php

use Illuminate\Database\Seeder;

class RevisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert revisions data into the table...
        DB::table('revisions')->insert([
            'rev_name' => 'A',
            'rev_name_notes' => 'Unapproved revision.',
        ]);

        DB::table('revisions')->insert([
            'rev_name' => 'B',
            'rev_name_notes' => 'Unapproved revision.',
        ]);

        DB::table('revisions')->insert([
            'rev_name' => '0',
            'rev_name_notes' => 'Approved revision.',
        ]);

        DB::table('revisions')->insert([
            'rev_name' => '1',
            'rev_name_notes' => 'Approved revision.',
        ]);
    }
}
/*
    $table->string('rev_name');
    $table->longText('rev_name_notes');
*/