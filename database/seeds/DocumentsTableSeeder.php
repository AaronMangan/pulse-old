<?php

use Illuminate\Database\Seeder;
use App\Documents;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate fake documents to populate the database..
        // $documents = factory(Documents::class, 10)->create();
        $metadata = collect(['author' => 'Aaron Mangan', 'Status' => 'DRAFT', 'cutom_field' => 'custom_data'])->toJson();

        DB::table('documents')->insert([
            'doc_title' => '2019 Financial Report - Technical Projects',
            'doc_rev' => 'A',
            'doc_status' => '1',
            'doc_type' => '1',
            'doc_discipline' => '1',
            'added_by_user' => '1',
            'metadata' => $metadata, 
        ]);

        DB::table('documents')->insert([
            'doc_title' => 'Health and Safety Management Plan',
            'doc_rev' => 'A',
            'doc_status' => '1',
            'doc_type' => '2',
            'doc_discipline' => '2',
            'added_by_user' => '1',
            'metadata' => $metadata, 
        ]);
    }
}

/**
 * // Standard fields...
 * $table->bigIncrements('id');
 * $table->timestamps();
 * $table->string('doc_class');
 * $table->string('doc_discipline');
 * $table->string('doc_type');
 * $table->string('doc_rev');
 * $table->string('doc_title');
 * $table->json('metadata');
 */