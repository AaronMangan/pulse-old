<?php

use Illuminate\Database\Seeder;

class DocTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed items into the database table...
        DB::table('doc_types')->insert([
            'doc_type_name' => 'Report',
            'doc_type_description' => 'A Report type document.',
            'added_by_user' => '1',
            'sort_order' => '0',
            'type_notes' => 'A report document, for example, progress reports, annual reports, board reports.',
        ]);

        DB::table('doc_types')->insert([
            'doc_type_name' => 'Plan',
            'doc_type_description' => 'A plan type document',
            'added_by_user' => '1',
            'sort_order' => '0',
            'type_notes' => 'Plan can include items such as Management Plan, Quality Control Plan, Project Execution Plan.',
        ]);

        DB::table('doc_types')->insert([
            'doc_type_name' => 'Schedule',
            'doc_type_description' => 'A Schedule type document.',
            'added_by_user' => '1',
            'sort_order' => '0',
            'type_notes' => 'A schedule document, for example, Project Schedule.',
        ]);
    }
}

/*
    // Standard fields...
    $table->bigIncrements('id');
    $table->timestamps();

    $table->string('doc_type_name');
    $table->string('doc_type_description');
    $table->integer('added_by_user');
    $table->integer('sort_order');
    $table->longText('type_notes');
*/