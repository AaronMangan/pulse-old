<?php

use Illuminate\Database\Seeder;

class HistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed data to the history table...
        DB::table('document_history')->insert([
            'document_id' => '1',
            'old_doc_status' => 'DRAFT', 
            'new_doc_status' => 'APPROVED',
            'old_doc_rev' => 'A',
            'new_doc_rev' => '0',
            'old_doc_type' => 'Report',
            'new_doc_type' => 'Plan',
            'old_doc_discipline' => 'Corporate',
            'new_doc_discipline' => 'Health & Safety',
            'old_doc_title' => 'Example Document - For Testing Only',
            'new_doc_title' => 'Example Document - For Testing Only',
            'old_doc_attachment' => '',
            'new_doc_attachment' => '',
            'updated_by_user' => '0',
        ]);
    }
}
