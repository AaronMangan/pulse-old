<?php

use Illuminate\Database\Seeder;
use App\User;

class PrefSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We need the currently logged in user information.
        $user = auth()->user();

        // Seed the user preferences table. This allows the user to establish their preferences.
        DB::table('user_prefs')->insert([
            'paginate_results_documents' => '50',
            'paginate_results_other' => '10',
            'collect_user_history' => false,
            'user_id' => '1',
        ]);
    }
}
