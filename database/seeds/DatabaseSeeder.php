<?php

use Illuminate\Database\Seeder;
use App\Documents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(DocumentsTableSeeder::class); // Not currently working..
         // $this->call(DocTypeSeeder::class);
         // $this->call(DisciplineSeeder::class);
         // $this->call(DocStatusSeeder::class);
         // $this->call(RevisionsSeeder::class);
         // $this->call(HistorySeeder::class);
         // $this->call(CompanyTableSeeder::class);
         // $this->call(TransmittalSeeder::class);
         // $this->call(UserTableSeeder::class);
         $this->call(PrefSeeder::class);
    }
}
