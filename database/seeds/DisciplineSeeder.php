<?php

use Illuminate\Database\Seeder;

class DisciplineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed the database with disciplines to use...
        DB::table('disciplines')->insert([
            'doc_discipline_name' => 'Corporate',
            'doc_discipline_description' => 'Documents belonging to a corporate function or team',
            'doc_discipline_notes' => 'Corporate documents examples: Business Management Plan OR Travel Request Forms used by the whole company',
            'added_by_user' => '1',
            'sort_order' => '0',
        ]);

        DB::table('disciplines')->insert([
            'doc_discipline_name' => 'Health & Safety',
            'doc_discipline_description' => 'Documents belonging to a health and safety team',
            'doc_discipline_notes' => 'Health & Safety documents examples include: Health & Safety Management Plan OR Incident Forms used by the whole company',
            'added_by_user' => '1',
            'sort_order' => '0',
        ]);

        DB::table('disciplines')->insert([
            'doc_discipline_name' => 'Quality',
            'doc_discipline_description' => 'Documents belonging to a quslity management/control team',
            'doc_discipline_notes' => 'Quality documents examples include: Quality Assurance Management Plan OR Quality Notice used by the whole company',
            'added_by_user' => '1',
            'sort_order' => '0',
        ]);
    }
}

/*
    $table->string('doc_discipline_name');
    $table->string('doc_discipline_description');
    $table->longText('doc_discipline_notes');
    $table->integer('added_by_user');
    $table->integer('sort_order');
*/